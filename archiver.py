from gitea import Gitea, Organization
import os

ARCHIVE_ORG = "archive"

def set_if_needed(item, attr, value):
    if getattr(item, attr) != value:
        setattr(item, attr, value)

def main():
    gitea = Gitea(os.environ["GITEA_URL"], os.environ["GITEA_TOKEN"])

    org = Organization.request(gitea, ARCHIVE_ORG)
    print("Fetching repositories...")
    for repo in org.get_repositories():
        set_if_needed(repo, "archived", True)
        set_if_needed(repo, "has_projects", False)
        set_if_needed(repo, "has_issues", False)
        set_if_needed(repo, "has_pull_requests", False)
        set_if_needed(repo, "has_wiki", False)

        if repo.get_dirty_fields():
            print("Updating", repo.get_full_name())
            repo.commit()


if __name__ == "__main__":
    main()
